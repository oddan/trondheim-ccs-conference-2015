%% Preliminaries
[Gt, rock] = getFormationTopGrid('johansenfm', 1);
ta         = trapAnalysis(Gt, false);

%% Figure 1 (traps and trap regions)

% colormap lines;
% regs = ta.trap_regions;
% regs(regs==0) = nan;
% traps = ta.traps;
% traps(traps==0) = nan;
% plotFaces(Gt, 1:Gt.cells.num, [0.9 0.9 0.9], 'edgealpha', 0);
% plotCellData(Gt, regs, 'edgealpha', 0.0);
% plotFaces(Gt, find(~isnan(traps)), [0 0 0], 'facealpha', 0.5, 'edgealpha', 0);
% axis off;
% set(gcf, 'position', [700 200 620 1200]);
% set(gcf, 'color', [1 1 1]);
% title('Spill regions')		
% set(get(gca, 'title'), 'FontSize', 30)	
% set(get(gca, 'title'), 'FontWeight', 'bold')

h = interactiveTrapping('Johansenfm', 'coarsening', 1, ...
   'method', 'node', 'light', true, 'contour', false);
view(-70, 80)
set(gcf, 'color', [1 1 1]);


%% Figure 2 (rivers and isonlines)
figure;
mapPlot(gcf, Gt, 'traps', ta.traps, 'rivers', ta.cell_lines);
axis off;
set(gcf, 'position', [700 200 1200 1200]);
set(gcf, 'color', [1 1 1]);
title('Traps and spill paths')		
set(get(gca, 'title'), 'FontSize', 30)	
set(get(gca, 'title'), 'FontWeight', 'bold')