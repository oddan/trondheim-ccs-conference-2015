% Run Bryne injection scenario with two wells, no dissoluton
[Gt, initState, states, wellSols, reports] = BryneInjection;

% Generate plots
directSelectedResultsMultiplot(Gt, reports, [21 151 221], 'plot_traps', true, ...
                               'background', 'totalCO2', 'plot_distrib', true);

% Run Bryne injection scenario with two wells and dissolution
[Gt_diss, initState_diss, states_diss, wellSols_diss, reports_diss] = ...
    BryneInjection('dissolution', true);

% Generate plots
directSelectedResultsMultiplot(Gt_diss, reports_diss, [21 151 221], 'plot_traps', true, ...
                               'background', 'totalCO2', 'plot_distrib', true);
