function [Gt, initState, states, wellSols, reports] = BryneInjection(varargin)

   moduleCheck('co2lab', 'ad-fi', 'ad-core');
   gravity reset on;
   
   opt.coarsening  = 2;
   opt.dissolution = false;
   opt.dis_rate    = 8.6924e-11;
   opt.dis_max     = 0.07;
   opt.cw          = 4.3e-5 / barsa; % linear wate compressibility
   opt.p_range     = [0.1, 400] * mega * Pascal; % CO2 default pressure range
   opt.t_range     = [  4, 250] + 274;           % CO2 default temperature range
   opt.res_vals    = [.11, .21];
   opt.temp_grad   = 30 / (kilo*meter); %thermal gradient
   opt.surf_temp   = 4; % in celsius
   opt.inj_point   = [5.7e5, 6.42e6; ...
                      6.25e5, 6.35e6; ...
                      5.4e5, 6.6e6];
   opt.inj_rate    = 20 * 1e6 * 1e3 / year; % mass per year (30 megatonnes)
   opt.inj_steps   = 20;
   opt.mig_steps   = 200;
   opt.inj_period  = 60 * year;
   opt.mig_period  = 1000 * year;

   opt = merge_options(opt, varargin{:});
   
   %% Load model
   
   % Setting up grid structure
   [gr, ~, petroinfo] = getAtlasGrid('Brynefm', 'coarsening', opt.coarsening);
   G = processGRDECL(gr{:});
   Gt = topSurfaceGrid(G);
   Gt = computeGeometry(Gt);
   ta = trapAnalysis(Gt, 'false');

   % Setting up rock structure
   rock.perm = petroinfo{1}.avgperm * ones(Gt.cells.num, 1);
   rock.poro = petroinfo{1}.avgporo * ones(Gt.cells.num, 1);
   
   %% Computing temperature regime
   T = Gt.cells.z * opt.temp_grad + (273.15 + opt.surf_temp);
   
   %% Setup fluid and rock
   fluid = makeVEFluid(Gt, rock, 'sharp interface', ...
                       'fixedT', T, ...
                       'co2_rho_pvt', [opt.p_range, opt.t_range], ...
                       'wat_rho_pvt', [opt.cw, 100 * barsa], ...
                       'dissolution', opt.dissolution, ...
                       'dis_rate', opt.dis_rate, ...
                       'dis_max', opt.dis_max, ...
                       'residual', opt.res_vals);
   
   %% Setup initial state
   
   nc = Gt.cells.num;
   initState = struct('pressure', norm(gravity) * fluid.rhoWS * Gt.cells.z, ...
                      's'       , [ones(nc, 1), zeros(nc, 1)], ...
                      'sGmax'   , zeros(nc, 1));
   if (opt.dissolution)
      initState.rs = zeros(nc, 1);
   end
   
   %% Setup wells, boundary conditions and schedule

   % Injection wells
   num_wells = size(opt.inj_point, 1);
   W = [];
   for i = 1:num_wells
      d = bsxfun(@minus, Gt.cells.centroids, opt.inj_point(i,:));
      [~, inj_ix] = min(sum(d.^2, 2));
      W = addWell(W, Gt, rock, inj_ix, ...
                  'type', 'rate', ...
                  'Comp_i', [0, 1], ...
                  'radius', 0.3, ...
                  'val', opt.inj_rate/ fluid.rhoGS);
   end
      
   W_shut = W;
   for i = 1:num_wells
      W_shut(i).val = 0;
   end
   
   
   % Boundary conditions (constant pressure)
   bfaces = find(prod(Gt.faces.neighbors, 2)==0);
   bcells = sum(Gt.faces.neighbors(bfaces,:), 2);
   bc = addBC([], bfaces, 'pressure', initState.pressure(bcells), 'sat', [1 0]);
   
   schedule.control = [struct('bc', bc, 'W', W); struct('bc', bc, 'W', W_shut)];
   
   dt_inj = opt.inj_period / opt.inj_steps;
   dt_mig = opt.mig_period / opt.mig_steps;
   schedule.step = struct('control', [     ones(opt.inj_steps, 1); ...
                                      2  * ones(opt.mig_steps, 1)], ...
                          'val', [dt_inj * ones(opt.inj_steps, 1); ...
                                  dt_mig * ones(opt.mig_steps, 1)]);
   
   %% Setup and run model
   
   model = CO2VEBlackOilTypeModel(Gt, rock, fluid);
   [wellSols, states] = simulateScheduleAD(initState, model, schedule);
   
   %% Prepare result structure for visualization
   reports = makeReports(Gt, {initState, states{:}}, rock, fluid, schedule, ...
                         opt.res_vals, ta, []);

end
